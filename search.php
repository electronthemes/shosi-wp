<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package SHOSI_-_Elegant_Blog_Theme
 */

get_header(); ?>
<section id="primary" class="content-area">
      <main id="main" class="site-main">
			<div class="container">
				<header class="page-header">
					<h1 class="page-title"><?php
						/* translators: %s: search query. */
						printf( esc_html__( 'Search Results for: %s', 'wp_shosi' ), '<span class="search-keyword">' . get_search_query() . '</span>' );
					?></h1>
				</header>
			</div>
            <!--==========================================================-->
            <!-- Page Content area start -->
            <!--==========================================================-->
            <div class="page-content">
			<?php 
			$sidebar_position = 'right'; // by default right
			if( get_theme_mod( 'search_sidebar_position' ) ){
				$sidebar_position = get_theme_mod( 'search_sidebar_position');
			}
			echo get_template_part( 'template-parts/post-loop-sidebar', $sidebar_position ); ?>
            </div>
            <!--==========================================================-->
            <!-- Page Content area End -->
            <!--==========================================================-->
      </main>
</section>
<?php get_footer(); ?>