<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SHOSI_-_Elegant_Blog_Theme
 */
get_header(); ?>
<section id="primary" class="content-area">
      <main id="main" class="site-main">
         <div class="container">
            <header class="page-header">
               <?php
                  the_archive_title( '<h1 class="page-title">', '</h1>' );
                  the_archive_description( '<div class="archive-description">', '</div>' );
               ?>
            </header>
         </div>
            <!--==========================================================-->
            <!-- Page Content area start -->
            <!--==========================================================-->
            <div class="page-content">
               <?php 
               $sidebar_position = 'left'; // by default right
               if( get_theme_mod( 'archieve_sidebar_position' ) ){
                  $sidebar_position = get_theme_mod( 'archieve_sidebar_position');
               }
               echo get_template_part( 'template-parts/post-loop-sidebar', $sidebar_position ); ?>
            </div>
            <!--==========================================================-->
            <!-- Page Content area End -->
            <!--==========================================================-->
      </main>
</section>
<?php get_footer(); ?>