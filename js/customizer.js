/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {


	// Header Section
	wp.customize( 'shosi_logo_type', function( value ) {
		value.bind( function( to ) {
			if( to == 'text_logo' ){
				$('.text-logo').show();
				$('.image-logo').hide();
			}else{
				$('.text-logo').hide();
				$('.image-logo').show();
			}
		} );
	} );

	wp.customize( 'display_tagline', function( value ) {
		value.bind( function( to ) {
			( true == to ) ? $('.shosi-desc').show() : $('.shosi-desc').hide();
		} );
	} );


	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$('.logo h1').html( to );
		} );
	} );	

	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$('.shosi-desc').html( to );
		} );
	} );

	wp.customize( 'shosiwp_link_color', function( value ) {
		value.bind( function( to ) {
			$('a').css('color' , to);
		} );
	} );

	// Footer Social Icons
	wp.customize( 'enable_footer_social_icons', function( value ) {
		value.bind( function( to ) {
			( true == to ) ? $('#footer-social-icons').show() : $('#footer-social-icons').hide();
		} );
	} );

} )( jQuery );