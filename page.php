<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 * 
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SHOSI_-_Elegant_Blog_Theme
 */
get_header(); ?>
<section id="primary" class="content-area">
      <main id="main" class="site-main">
    <!--==========================================================-->
      <!-- Page Content area start -->
      <!--==========================================================-->
    <?php 
    $sidebar_position = 'right'; // by default right
    if ( get_theme_mod( 'page_sidebar_position' ) ) {
      $sidebar_position = get_theme_mod( 'page_sidebar_position' );
    }
    echo get_template_part( 'template-parts/content-page-sidebar' , $sidebar_position ); ?>
      <!--==========================================================-->
      <!-- Page Content area End -->
      <!--==========================================================-->
  </main>
</section>
<?php get_footer(); ?>