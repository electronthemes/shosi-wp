<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SHOSI_-_Elegant_Blog_Theme
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">
	<div id="content" class="site-content">
      <header class="main-header">
         <div class="logo container">
            <?php 
            $logo_type = get_theme_mod( 'shosi_logo_type' , 'text_logo' );?>
            <div class="text-logo" style="display: <?php echo $logo_type == 'text_logo' ? 'block' : 'none'; ?>">
               <a href="<?php echo esc_url( get_home_url() ); ?>">
                  <h1><?php bloginfo( 'name' ); ?></h1>
                  <h3 class="shosi-desc" style="display: <?php echo get_theme_mod( 'display_tagline' ,  false ) ? 'block' : 'none'; ?>"><?php bloginfo('description') ?></h3>
               </a>
            </div>

            <div class="image-logo" style="display: <?php echo $logo_type == 'image_logo' ? 'block' : 'none'; ?>">
               <?php if (get_theme_mod( 'custom_logo' )) { ?>
                  <img src="<?php $logo_id = get_theme_mod( 'custom_logo' ); $src = wp_get_attachment_image_src( $logo_id , 'full' ); echo $src[0]; ?>" alt="<?php bloginfo('description') ?>">
               <?php } else { ?>
                  <div class="alert alert-danger">
                     <b><?php esc_html_e( 'Uploload logo image' , 'wp_shosi' ); ?></b>
                     <p><?php esc_html_e( 'Appearence > Customize > Site Identity > Logo' , 'wp_shosi' ); ?></p>
                  </div>
               <?php } ?>
               
            </div>
         </div>
         <!--==========================================================-->
         <!-- Main Menu start -->
         <!--==========================================================-->
         <div class="menu">
            <div class="container">
               <div class="main-menu">
                  <nav id="nav">
                     <?php    
                     /**
                        * Displays a navigation menu
                        * @param array $args Arguments
                     */
                        $args = array(
                           'theme_location' => 'menu-1',
                           'container' => 'ul',
                           'menu_class' => 'menu_wrapper',

                        );
                        wp_nav_menu( $args ); ?>
                  </nav>
               </div>
            </div>
         </div><!-- .menu -->
         <!--==========================================================-->
         <!-- Main Menu end -->
         <!--==========================================================-->
         <!--==========================================================-->
         <!-- Mobile Menu start -->
         <!--==========================================================-->
         <nav class="mobile-menu hidden">
            <?php    
            /**
               * Displays a navigation menu
               * @param array $args Arguments
            */
               $args = array(
                  'theme_location' => 'menu-1',
                  'container' => 'ul',
                  'menu_class' => 'menu_wrapper',
               );
            
               wp_nav_menu( $args ); ?>
         </nav><!-- .mobile-menu.hidden -->
         <!--==========================================================-->
         <!-- Mobile Menu end -->
         <!--==========================================================-->
      </header>