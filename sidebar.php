<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SHOSI_-_Elegant_Blog_Theme
 */
if ( ! is_active_sidebar( 'sidebar' ) ) {
	return;
}
?>

<!--==========================================================-->
<!-- Sidebar area start -->
<!--==========================================================-->
<div class="sidebar">
 <!-- Single widget start -->
    <?php dynamic_sidebar( 'sidebar' ) ?>
 <!-- Single widget end -->
</div>
<!--.sidebar-->
<!--==========================================================-->
<!-- Sidebar area end -->
<!--==========================================================-->