<?php get_header(); ?>
<section id="primary" class="content-area">
      <main id="main" class="site-main">
            <!--==========================================================-->
            <!-- Page Content area start -->
            <!--==========================================================-->
            <div class="page-content">
      			<?php 
      			$sidebar_position = 'left'; // by default right
      			if( get_theme_mod( 'homepage_sidebar_position' ) ){
      				$sidebar_position = get_theme_mod( 'homepage_sidebar_position');
      			}
      			echo get_template_part( 'template-parts/post-loop-sidebar', $sidebar_position ); 
      			?>
            </div>
            <!--==========================================================-->
            <!-- Page Content area End -->
            <!--==========================================================-->
      </main>
</section>
<?php get_footer(); ?>