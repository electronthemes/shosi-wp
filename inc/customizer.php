<?php
/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function wp_shosi_customize_register( $wp_customize ) {

	/**
	 * -------------------------------------------------
	 * 		Site Identity
	 * -------------------------------------------------
	 */	
     $wp_customize->add_setting('shosi_logo_type', array(
        'default'        => 'text_logo',
        'capability'     => 'edit_theme_options',
        'transport'      => 'postMessage'
 
    ));
    $wp_customize->add_control( 'shosi_logo_type', array(
        'label'   => __('Logo Type','wp_shosi'),
        'section' => 'title_tagline',
        'priority' => 7,
        'type'    => 'select',
        'choices'    => array(
            'text_logo' => __('Text Logo' , 'wp_shosi'),
            'image_logo' => __('Image Logo' , 'wp_shosi')
        ),
    ));
    $wp_customize->remove_control('display_header_text');


    $wp_customize->add_setting('display_tagline' , array(
    	'default' => false,
    	'transport' => 'postMessage'
    ));
    $wp_customize->add_control('display_tagline' , array(
    	'label' => __('Display Tagline' , 'wp_shosi'),
    	'type' => 'checkbox',
    	'section' => 'title_tagline'
    ));


	/**
	 * -------------------------------------------------
	 * 		Sidebar Position
	 * -------------------------------------------------
	 */	
	$wp_customize->add_section('Sidebar_position' , array(
		'title' => __('Sidebar Position' , 'wp_shosi')
	));

	//HomePage sidebar
	$wp_customize->add_setting('homepage_sidebar_position' , array(
		'default' => 'right'
	));
	$wp_customize->add_control('homepage_sidebar_position' , array(
		'label' => __('Home Sidebar Position','wp_shosi'),
		'type' => 'radio',
		'section' => 'Sidebar_position',
		'choices' => array(
			'left' => __('Left' , 'wp_shosi'),
			'right' => __('Right' , 'wp_shosi'),
		)
	));

	//Single sidebar
	$wp_customize->add_setting('singlepage_sidebar_position' , array(
		'default' => 'right'
	));
	$wp_customize->add_control('singlepage_sidebar_position' , array(
		'label' => __('Post Sidebar Position','wp_shosi'),
		'type' => 'radio',
		'section' => 'Sidebar_position',
		'choices' => array(
			'left' => __('Left' , 'wp_shosi'),
			'right' => __('Right' , 'wp_shosi'),
		)
	));

	//Page sidebar
	$wp_customize->add_setting('page_sidebar_position' , array(
		'default' => 'right'
	));
	$wp_customize->add_control('page_sidebar_position' , array(
		'label' => __('Page Sidebar Position','wp_shosi'),
		'type' => 'radio',
		'section' => 'Sidebar_position',
		'choices' => array(
			'left' => __('Left' , 'wp_shosi'),
			'right' => __('Right' , 'wp_shosi'),
		)
	));
	//Archieve sidebar
	$wp_customize->add_setting('archieve_sidebar_position' , array(
		'default' => 'right'
	));
	$wp_customize->add_control('archieve_sidebar_position' , array(
		'label' => __('Archieve Sidebar Position','wp_shosi'),
		'type' => 'radio',
		'section' => 'Sidebar_position',
		'choices' => array(
			'left' => __('Left' , 'wp_shosi'),
			'right' => __('Right' , 'wp_shosi'),
		)
	));

	//Search sidebar
	$wp_customize->add_setting('search_sidebar_position' , array(
		'default' => 'right'
	));
	$wp_customize->add_control('search_sidebar_position' , array(
		'label' => __('Search Sidebar Position','wp_shosi'),
		'type' => 'radio',
		'section' => 'Sidebar_position',
		'choices' => array(
			'left' => __('Left' , 'wp_shosi'),
			'right' => __('Right' , 'wp_shosi'),
		)
	));


	 /**
	 * -------------------------------------------------
	 * 		Featured Section
	 * -------------------------------------------------
	 */

	 $wp_customize->add_section('featured_posts_section' , array(
	 	'title' => __('Featured Posts Section' , 'wp_shosi')
	 ));
	 $wp_customize->add_setting('featured_post_category' , array( 'default' => 'uncategorized' ));

 	$categories = get_categories();
	$cats = array();
	$i = 0;
	foreach($categories as $category){
		if($i==0){
			$default = $category->slug;
			$i++;
		}
		$cats[$category->slug] = $category->name;
	}
	 $wp_customize->add_control('featured_post_category' , array(
	 	 'label' => __('Select Featured Post Category' , 'wp_shosi'),
	 	 'type' => 'select',
	 	 'section' => 'featured_posts_section',
	 	 'choices' => $cats
	 ));

	 // Featured Post cout
	 $wp_customize->add_setting('fet_post_count' , array(
	 	'default' => 3
	 ));
	 $wp_customize->add_control('fet_post_count' , array(
	 	'type' => 'number',
	 	'section' => 'featured_posts_section',
	 	'label' => __('Featured post count' , 'wp_shosi'),
	 	'input_attrs' => array(
	 		'style' => 'width: 55px'
	 	)
	 ));

	 // Show in homepage
	 $wp_customize->add_setting('show_fet_posts_in_homepage' , array( 'default' => true ));
	 $wp_customize->add_control('show_fet_posts_in_homepage' , array(
	 	'label' => __('Show in Home' , 'wp_shosi'),
	 	'section' => 'featured_posts_section',
	 	'type' => 'checkbox'
	 ));


	 // Show in post
	 $wp_customize->add_setting('show_fet_posts_in_post' , array( 'default' => true ));
	 $wp_customize->add_control('show_fet_posts_in_post' , array(
	 	'label' => __('Show in Post' , 'wp_shosi'),
	 	'section' => 'featured_posts_section',
	 	'type' => 'checkbox'
	 ));

	 // Show in Archieve
	 $wp_customize->add_setting('show_fet_posts_in_archieve' , array( 'default' => true ));
	 $wp_customize->add_control('show_fet_posts_in_archieve' , array(
	 	'label' => __('Show in Archieve' , 'wp_shosi'),
	 	'section' => 'featured_posts_section',
	 	'type' => 'checkbox'
	 ));

	 // Show in Search
	 $wp_customize->add_setting('show_fet_posts_in_search' , array( 'default' => true ));
	 $wp_customize->add_control('show_fet_posts_in_search' , array(
	 	'label' => __('Show in Search' , 'wp_shosi'),
	 	'section' => 'featured_posts_section',
	 	'type' => 'checkbox'
	 ));




	 /**
	 * -------------------------------------------------
	 * 		Colors
	 * -------------------------------------------------
	 */
	$wp_customize->remove_control('header_textcolor');
	$wp_customize->get_section('colors')->title = 'Color Schemes';

	$wp_customize->add_setting( 'shosi_color_scheme', array(
	  'capability' => 'edit_theme_options',
	  'default' => 'primary'
	) );

	$wp_customize->add_control( 'shosi_color_scheme', array(
	  'type' => 'select',
	  'section' => 'colors',
	  'label' => __( 'Color Schemes' , 'wp_shosi' ),
	  'description' => __( 'Select Theme Main Color Scheme' , 'wp_shosi' ),
	  'choices' => array(
		    'blue' => __( 'blue' ),
		    'brown' => __( 'brown' ),
		    'cyan' => __( 'cyan' ),
		    'primary' => __( 'primary' ),
		    'green' => __( 'green' ),
		    'grey-blue' => __( 'grey-blue' ),
		    'indigo' => __( 'indigo' ),
	  ),
	) );

	/**
	 * -------------------------------------------
	 * 		Instagram Configuration
	 * -------------------------------------------
	 */
	// Register a new section
	// $wp_customize->add_section('insta_config',array(
	// 	'title' => 'Instagram Configuration'
	// ));
	// $wp_customize->add_setting('insta_section_title',array(
	// 	"default" => "Follow Me On #king_rayhan",
	// 	"transport" => "refresh"
	// ));
	// $wp_customize->add_control('insta_section_title',array(
	// 	'label' => __('Section Title','wp_shosi'),
	// 	'type'  => 'text',
	// 	'section' => 'insta_config'
	// ));

	$wp_customize->add_setting('insta_user_id',array(
		//"default" => "shosiwp Wordpress theme by <a href='//rayhan.info' target='_blank'>@KingRayhan</a>",
		"transport" => "refresh"
	));
	$wp_customize->add_control('insta_user_id',array(
		'label' => __('User Id','wp_shosi'),
		'description' => 'Note that <code>YOUR_USER_ID</code> corresponds to your Instagram account ID (eg: 4385108), not your username.<br>Get your instagram userid from <a target="_blank" href="//smashballoon.com/instagram-feed/find-instagram-user-id/">here</a>',
		'type'  => 'text',
		'section' => 'insta_config'
	));

	$wp_customize->add_setting('insta_client_id',array(
		"default" => "shosiwp Wordpress theme by <a href='//rayhan.info' target='_blank'>@KingRayhan</a>",
		"transport" => "refresh"
	));
	$wp_customize->add_control('insta_client_id',array(
		'label' => __('Access Token','wp_shosi'),
		'type'  => 'text',
		'section' => 'insta_config'
	));

	$wp_customize->add_setting('insta_image_limit',array(
		"default" => "8",
		"transport" => "refresh"
	));
	$wp_customize->add_control('insta_image_limit',array(
		'label' => __('Image Limit','wp_shosi'),
		'type'  => 'text',
		'section' => 'insta_config'
	));





	/**
	 * -------------------------------------------
	 * 		Footer
	 * -------------------------------------------
	 */
	// Register a new section
	$wp_customize->add_panel('footer_panel',array(
		'title' => __('Footer','wp_shosi'),
		'priority' => 99999999
	));


	// Footer Copyright
	$wp_customize->add_section( 'footer_copyright_section' , array(
		'title' => __('Footer Copyright' , 'wp_shosi'),
		'panel' => 'footer_panel'
	));
	$wp_customize->add_setting('footer_copyright',array(
		"default" => esc_html__( '© Copyright '.date('Y').' - <b>SHOSi</b> - Clean html blog Template. Designed by <span><i class="fa fa-heart"></i></span> <b><a href="//electronthemes.xyz" target="_blank">ElectronThemes</a></b>' , 'wp_shosi' ),
		"transport" => "refresh"
	));
	$wp_customize->add_control('footer_copyright',array(
		'label' => __('Footer copyright text','wp_shosi'),
		'type'  => 'textarea',
		'section' => 'footer_copyright_section'
	));




	//Social icon section
	$wp_customize->add_section('footer_social_icons' , array( 
		'title' => __('Social icons' , 'wp_shosi'),
		'panel' => 'footer_panel'
	));


	$wp_customize->add_setting('enable_footer_social_icons' , array(
		'default' => true,
		'transport' => 'postMessage'
	));
	$wp_customize->add_control('enable_footer_social_icons' , array(
		'label' => __('Enable footer social icons' , 'wp_shosi'),
		'type' => 'checkbox',
		'section' => 'footer_social_icons'
	));



	$wp_customize->add_setting('social_icons',array(
		'transport' => 'refresh'
	));
	$wp_customize->add_control('social_icons',array(
		'section' => 'footer_social_icons',
		'type' => 'hidden',
		'label' => __('','wp_shosi'),
		'description' => __("<h2>Social Icons</h2>Give your social profile url which icons you want to show in footer. <br/><b>NOTE:</b> you must have to put url with <code>http://</code> or <code>https://</code>",'wp_shosi')
	));

	/*-----------------------------------------------------------*
	 * Defining our own 'Social Links' section
	 *-----------------------------------------------------------*/
	/* Twitter URL */
	$wp_customize->add_setting(
		'shosiwp_social_twitter',
		array(
			'default'            => '',
			'transport'          => 'refresh'
		)
	);
	$wp_customize->add_control(
		'shosiwp_social_twitter',
		array(
			'section'  => 'footer_social_icons',
			'label'    => __('Twitter','wp_shosi'),
			'type'     => 'text'
		)
	);
	/* Facebook URL */
	$wp_customize->add_setting(
		'shosiwp_social_facebook',
		array(
			'default'            => '',
			'transport'          => 'refresh'
		)
	);
	$wp_customize->add_control(
		'shosiwp_social_facebook',
		array(
			'section'  => 'footer_social_icons',
			'label'    => __('Facebook','wp_shosi'),
			'type'     => 'text'
		)
	);
	/* Google URL */
	$wp_customize->add_setting(
		'shosiwp_social_google',
		array(
			'default'            => '',
			'transport'          => 'refresh'
		)
	);
	$wp_customize->add_control(
		'shosiwp_social_google',
		array(
			'section'  => 'footer_social_icons',
			'label'    => __('Google+','wp_shosi'),
			'type'     => 'text'
		)
	);
	/* Pinterest URL */
	$wp_customize->add_setting(
		'shosiwp_social_pinterest',
		array(
			'default'            => '',
			'transport'          => 'refresh'
		)
	);
	$wp_customize->add_control(
		'shosiwp_social_pinterest',
		array(
			'section'  => 'footer_social_icons',
			'label'    => __('Pinterest','wp_shosi'),
			'type'     => 'text'
		)
	);
	/* Linkedin URL */
	$wp_customize->add_setting(
		'shosiwp_social_linkedin',
		array(
			'default'            => '',
			'transport'          => 'refresh'
		)
	);
	$wp_customize->add_control(
		'shosiwp_social_linkedin',
		array(
			'section'  => 'footer_social_icons',
			'label'    => __('Linkedin','wp_shosi'),
			'type'     => 'text'
		)
	);
	/* Github URL */
	$wp_customize->add_setting(
		'shosiwp_social_github',
		array(
			'default'            => '',
			'transport'          => 'refresh'
		)
	);
	$wp_customize->add_control(
		'shosiwp_social_github',
		array(
			'section'  => 'footer_social_icons',
			'label'    => __('Github','wp_shosi'),
			'type'     => 'text'
		)
	);
	/* Instagram URL */
	$wp_customize->add_setting(
		'shosiwp_social_instagram',
		array(
			'default'            => '',
			'transport'          => 'refresh'
		)
	);
	$wp_customize->add_control(
		'shosiwp_social_instagram',
		array(
			'section'  => 'footer_social_icons',
			'label'    => __('Instagram','wp_shosi'),
			'type'     => 'text'
		)
	);
	/* Medium URL */
	$wp_customize->add_setting(
		'shosiwp_social_medium',
		array(
			'default'            => '',
			'transport'          => 'refresh'
		)
	);
	$wp_customize->add_control(
		'shosiwp_social_medium',
		array(
			'section'  => 'footer_social_icons',
			'label'    => __('Medium','wp_shosi'),
			'type'     => 'text'
		)
	);
	/* Vine URL */
	$wp_customize->add_setting(
		'shosiwp_social_vine',
		array(
			'default'            => '',
			'transport'          => 'refresh'
		)
	);
	$wp_customize->add_control(
		'shosiwp_social_vine',
		array(
			'section'  => 'footer_social_icons',
			'label'    => __('Vine','wp_shosi'),
			'type'     => 'text'
		)
	);
	/* Tumblr URL */
	$wp_customize->add_setting(
		'shosiwp_social_tumblr',
		array(
			'default'            => '',
			'transport'          => 'refresh'
		)
	);
	$wp_customize->add_control(
		'shosiwp_social_tumblr',
		array(
			'section'  => 'footer_social_icons',
			'label'    => __('Tumblr','wp_shosi'),
			'type'     => 'text'
		)
	);
	/* YouTube URL */
	$wp_customize->add_setting(
		'shosiwp_social_youtube',
		array(
			'default'            => '',
			'transport'          => 'refresh'
		)
	);
	$wp_customize->add_control(
		'shosiwp_social_youtube',
		array(
			'section'  => 'footer_social_icons',
			'label'    => __('YouTube','wp_shosi'),
			'type'     => 'text'
		)
	);


	$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->get_setting( 'background_color' )->transport = 'postMessage';

	$wp_customize->get_section('custom_css')->title = 'Custom CSS';
}
add_action( 'customize_register', 'wp_shosi_customize_register' );


/**
 * Used by hook: 'customize_preview_init'
 * 
 * @see add_action('customize_preview_init',$func)
 */
function shosi_customizer_live_preview()
{
	wp_enqueue_script( 
		  'shosi-customizer-js',			//Give the script an ID
		  get_template_directory_uri().'/js/customizer.js',//Point to file
		  array( 'jquery','customize-preview' ),	//Define dependencies
		  '1.0',						//Define a version (optional) 
		  true						//Put script in footer?
	);
}
add_action( 'customize_preview_init', 'shosi_customizer_live_preview' );