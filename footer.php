<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SHOSI_-_Elegant_Blog_Theme
 */
?>
	</div><!-- #content // started in header.php-->

	<section class="instagramPhoto">
	 <div class="container-fluid">
	    <div class="row">
	       <div class="follow-me text-center">
	          <h2><?php echo get_theme_mod('insta_section_title'); ?></h2>
	       </div>
	       <div id="instafeed"></div>
	       <div class="clearfix"></div>
	    </div>
	 </div>
	</section>
	<footer id="colophon" class="footer site-footer">
         <div class="container">
            <div class="row">
               <div class="col-xs-12">
                  <div class="clearfix"></div>
                  <ul class="footer-social-icons" id="footer-social-icons" style="display: <?php echo ( get_theme_mod( 'enable_footer_social_icons' ) ) ? 'block' : 'none' ; ?>">
<?php
echo ( get_theme_mod( 'shosiwp_social_twitter' ) ) ? '<li><a href="'. get_theme_mod( 'shosiwp_social_twitter' ) .'"><i class="fa fa-twitter"></i></a></li>' : '';
echo ( get_theme_mod( 'shosiwp_social_facebook' ) ) ? '<li><a href="'. get_theme_mod( 'shosiwp_social_facebook' ) .'"><i class="fa fa-facebook"></i></a></li>' : '';
echo ( get_theme_mod( 'shosiwp_social_google' ) ) ? '<li><a href="'. get_theme_mod( 'shosiwp_social_google' ) .'"><i class="fa fa-google-plus"></i></a></li>' : '';
echo ( get_theme_mod( 'shosiwp_social_pinterest' ) ) ? '<li><a href="'. get_theme_mod( 'shosiwp_social_pinterest' ) .'"><i class="fa fa-pinterest"></i></a></li>' : '';
echo ( get_theme_mod( 'shosiwp_social_linkedin' ) ) ? '<li><a href="'. get_theme_mod( 'shosiwp_social_linkedin' ) .'"><i class="fa fa-linkedin"></i></a></li>' : '';
echo ( get_theme_mod( 'shosiwp_social_github' ) ) ? '<li><a href="'. get_theme_mod( 'shosiwp_social_github' ) .'"><i class="fa fa-github"></i></a></li>' : '';
echo ( get_theme_mod( 'shosiwp_social_instagram' ) ) ? '<li><a href="'. get_theme_mod( 'shosiwp_social_instagram' ) .'"><i class="fa fa-instagram"></i></a></li>' : '';
echo ( get_theme_mod( 'shosiwp_social_medium' ) ) ? '<li><a href="'. get_theme_mod( 'shosiwp_social_medium' ) .'"><i class="fa fa-medium"></i></a></li>' : '';
echo ( get_theme_mod( 'shosiwp_social_vine' ) ) ? '<li><a href="'. get_theme_mod( 'shosiwp_social_vine' ) .'"><i class="fa fa-vine"></i></a></li>' : '';
echo ( get_theme_mod( 'shosiwp_social_tumblr' ) ) ? '<li><a href="'. get_theme_mod( 'shosiwp_social_tumblr' ) .'"><i class="fa fa-tumblr"></i></a></li>' : '';
echo ( get_theme_mod( 'shosiwp_social_youtube' ) ) ? '<li><a href="'. get_theme_mod( 'shosiwp_social_youtube' ) .'"><i class="fa fa-youtube"></i></a></li>' : '';
?>
                     
                  </ul>
                  <span class="copyright"><?php echo get_theme_mod('footer_copyright' , '© Copyright '.date('Y').' - <b>SHOSi</b> - Clean html blog Template. Designed by <span><i class="fa fa-heart"></i></span> <b><a href="//electronthemes.xyz" target="_blank">ElectronThemes</a></b>'); ?></span> 
               </div>
               <!-- end col-12 --> 
            </div>
            <!-- end row --> 
         </div>
         <!-- end container --> 
	</footer><!-- #colophon -->
</div><!-- #page // started in header.php-->
<?php wp_footer(); ?>
</body>
</html>
