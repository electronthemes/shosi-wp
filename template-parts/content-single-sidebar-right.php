      <div class="page-content">
        <div class="container">
            <div class="row">
	            <div class="col-md-8">
	               	<div class="single-page-content">
		               <?php
		               if ( have_posts() ) :

		                  /* Start the Loop */
		                  while ( have_posts() ) : the_post();

		                     get_template_part( 'template-parts/content', 'single' );

		                  endwhile;

		                  the_posts_navigation();

		               else :

		                  get_template_part( 'template-parts/content', 'none' );

		               endif; ?>
	                </div>
	            </div>
                <div class="col-md-4">
                   <?php get_sidebar(); ?>    
                </div>
            </div>
        </div>
      </div><!-- page-content END -->