<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package SHOSI_-_Elegant_Blog_Theme
 */
get_header(); ?>
<section id="primary" class="content-area">
      <main id="main" class="site-main">
	  <!--==========================================================-->
      <!-- Page Content area start -->
      <!--==========================================================-->
		<?php 
		$sidebar_position = 'right'; // by default right
		if ( get_theme_mod( 'singlepage_sidebar_position' ) ) {
			$sidebar_position = get_theme_mod( 'singlepage_sidebar_position' );
		}
		echo get_template_part( 'template-parts/content-single-sidebar' , $sidebar_position ); ?>
      <!--==========================================================-->
      <!-- Page Content area End -->
      <!--==========================================================-->
	</main>
</section>
<?php get_footer(); ?>
